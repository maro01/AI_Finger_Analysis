import cv2
import mediapipe as mp
import time


class handDetector():
    def __init__(self,mode=False,maxHands = 2, detectionCon = 0.5, trackCon = 0.5) :
        self.mode = mode
        self.maxHands = maxHands
        self.detectionCon = detectionCon
        self.trackCon = trackCon

        self.mpHands = mp.solutions.hands  # Suivi de la main humaine
        self.hands = self.mpHands.Hands(self.mode,self.maxHands,self.detectionCon,self.trackCon)  # variable de parametres de la main humaine
        self.mpDraw = mp.solutions.drawing_utils
        self.tipIds = [4,8,12,16,20] # les reperes des 5 doigts de la main humaine

    def findHands(self, img , draw = True):

         imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convertir l'image en RGB
         self.results = self.hands.process(imgRGB)  # Traitement de suivie de la main humaine avec l'RGB

        # if self.results.multi_hand_landmarks:  # si les reperes existent...
         #    for handLms in self.results.multi_hand_landmarks:  # pour chaque main humaine "handLms" dans "multi_hand_Landmarks"
          #       if draw:
           #          self.mpDraw.draw_landmarks(img, handLms,self.mpHands.HAND_CONNECTIONS)  # On affiche les 21 points de maine humaine connectes
         return img

    def findPosition(self, img , handNo=0 , draw=True):
       self.lmList = []
       if self.results.multi_hand_landmarks:  # si les reperes existent...
           myHand = self.results.multi_hand_landmarks[handNo]
           for id, lm in enumerate(myHand.landmark):  # enumerer l"ID" du point sur 21 points de la maine detectee et "lm" est son repere
              # print(id,lm) # conversion des reperes de "coordonnes decimaux" à des "coordoones de pixels" sur l'image de la WebCam
              h, w, c = img.shape
              cx, cy = int(lm.x * w), int(lm.y * h)  # calcul des X et Y du pixel de repere de la maine humaine
              #print(id, cx,cy)  # affichage de l"ID" du point sur 21 points de la maine detectee et ses reperes de pixel
              self.lmList.append([id,cx,cy])
              if draw:
                cv2.circle(img, (cx, cy), 15, (255, 0, 255), cv2.FILLED)
       return self.lmList

    def fingerUp(self):
        fingers = [] #liste des doigts

        #L'index est levé ou non ?
        if self.lmList[self.tipIds[0]][1] < self.lmList[self.tipIds[0] - 1][1]:
            fingers.append(1) #oui
        else:
            fingers.append(0) #non

        # les 4 autres doigts sont levés ou non ?
        for id in range(1,5):
            # on checke si les repères des autres 4 doigts s'ils sont en dessous de l'index
            # s'ils sont moins reperés, donc ils ne sont pas levés, sinon ...
            if self.lmList[self.tipIds[id]][2] < self.lmList[self.tipIds[id] - 2][2]:
                fingers.append(1)
            else:
                fingers.append(0)

        return fingers #retourner le doigt levé

# Lancement du script

def main():
    pTime = 0  # Temps precedent
    cTime = 0  # Temps courant
    cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # WebCam
    detector = handDetector()
    while True:
        success, img = cap.read()  # Cadre de generation de la WebCam
        img = detector.findHands(img)
        lmList = detector.findPosition(img)
        if len(lmList) != 0:
         print(lmList[8]) # tableau des 21 reperes de la main humaine

        cTime = time.time()  # affectation de temps courant
        fps = 1 / (cTime - pTime)  # calcul d'FPS par miliseconde
        pTime = cTime  # affectation du temps precedent

        cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_SIMPLEX,3, (255, 0, 255),
                    3)  # affichage de calcul d'FPS sur la WebCam
        cv2.imshow("Image", img)  # afficher la WebCam

        # Quitter la WebCam avec le bouton "q"
        if cv2.waitKey(1) & 0xFF == ord('q'):
           break
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()