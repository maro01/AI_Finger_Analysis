import cv2
import numpy as np
import time
import os
import HandTrackingModule as htm

################
brushThickness = 15   #Variable d'epaisseur du dessin
eraserThickness = 100  #Variable d'epaisseur de la gomme
################

folderPath = "Header"
myList = os.listdir(folderPath) # importer la liste des images de barre de taches
print(myList) #afficher la liste des images
overlayList = [] #liste dans laquelle on charge la liste des images de la barre de taches
for imPath in myList:
    image = cv2.imread(f'{folderPath}/{imPath}')
    overlayList.append(image)
print(len(overlayList)) # afficher la taille de la liste des images de la barre de taches

header = overlayList[0] # donner l'image n°0 à notre header comme une image par defaut
drawColor = (0,0,255) # Couleur de choix ( Rouge par défaut )

cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # WebCam
cap.set(3,1280) # regler la largeur de la webCam à 1280px
cap.set(4,720) # regler la hauteur de la webCam à 720px

detector = htm.handDetector(detectionCon=0.85) # importer methode de HandTrackingModule
xp, yp = 0, 0
#L'image de la WebCam ne peut pas laisser le dessin fixe à cause de son mise à jour instantané ,
# c'est pourquoi, on crée un Canvas en arrière plan qui garde l'objet dessiné.
imgCanvas = np.zeros((720,1280,3),np.uint8)

xp,yp = 0,0 # coordonnées de point précedent par rapport au mouvement de dessin

while True:

    ####Importer les images
    success, img = cap.read()
    img = cv2.flip(img, 1) #Transition gauche droite de la WebCam afin de faciliter le dessin

    ####Trouver les reperes d'une main humaine
    img = detector.findHands(img)
    lmList = detector.findPosition(img,draw=False) # charger tous les reperes des 21 points de la main humaine
    if len(lmList)!=0:
       #print(lmList)
       x1,y1 = lmList[8][1:] # les coordonnes d'index
       x2,y2 = lmList[12][1:] # les coordonnes du majeur
       ####Chercher un doigt levé
       fingers = detector.fingerUp()
       # print(fingers)

    ####Determinaison de geste de selection : si l'index et le majeur sont levés simultanément
       if fingers[1] and fingers[2]:
          # une fois on est en mode de selection , on réinitialise le point de départ pour résoudre le problème
          # de liasion entre deux dessins distincts
          xp, yp = 0, 0
          print("Mode de selection active")
          if y1 < 125:  # 125px est le cordonnees d'hauteur de la barre des taches ( si on touche dans la barre des taches)
             if 264<x1<381: # On clique sur le 1er choix
                 header = overlayList[0]
                 drawColor = (0,0,255)
             elif 497<x1<625: # On clique sur le 2eme choix
                 header = overlayList[1]
                 drawColor = (255, 255, 255)
             elif 746<x1<872: # On clique sur le 3eme choix
                 header = overlayList[2]
                 drawColor = (35, 177, 77)
             elif 1034 < x1 < 1184:  # On clique sur le 4eme choix
                 header = overlayList[3]
                 drawColor = (0, 0, 0)

         # cv2.rectangle(img, (x1, y1 - 25), (x2, y2 + 25), drawColor, cv2.FILLED)

       ######Activation du dessin quand un doigt est levé
       elif fingers[1] and fingers[2]==False:
            cv2.circle(img,(x1,y1),15, drawColor, cv2.FILLED)
            print("Mode de dessin active")
            if xp==0 and yp==0:  # S'il y a pas de point précédent , ça sera égal au point courant qui est le point initial
                 xp,yp = x1,y1
                # Dessiner une ligne entre Xp,Yp : point précédent et X1,Y1 : point courant

            if drawColor == (0,0,0): # si le couleur est noir , donc c'est une gomme
                cv2.line(img, (xp,yp),(x1,y1),drawColor,eraserThickness)
                cv2.line(imgCanvas, (xp,yp),(x1,y1),drawColor,eraserThickness)
            else:
                cv2.line(img, (xp,yp),(x1,y1),drawColor,brushThickness)
                cv2.line(imgCanvas, (xp,yp),(x1,y1),drawColor,brushThickness)

            xp, yp = x1, y1 #mise à jour du point précédent


    imgGray = cv2.cvtColor(imgCanvas,cv2.COLOR_BGR2GRAY) #On convertit l'image Canvas à une image grise
    _, imgInv =  cv2.threshold(imgGray,50,255,cv2.THRESH_BINARY_INV) # Convertir l'image grise en image inversée
    imgInv = cv2.cvtColor(imgInv,cv2.COLOR_GRAY2RGB)
    img = cv2.bitwise_and(img,imgInv)
    # On ajout le OR pour permettre d'afficher les dessins en couleur dans l'img au lieu de dessin noir
    img = cv2.bitwise_or(img,imgCanvas)

    ###### préparer l'image de barre des tâches
    img[0:125,0:1280] = header

    #img = cv2.addWeighted(img,0.5,imgCanvas,0.5,0) # fusionner la WebCam et le Canvas en même image

    cv2.imshow("Image",img)  # afficher la WebCam
    ##cv2.imshow("Inv",imgInv)  # afficher le Canvas

    # Quitter la WebCam avec le bouton "q"
    if cv2.waitKey(1) & 0xFF == ord('q'):
         break
cap.release()
cv2.destroyAllWindows()